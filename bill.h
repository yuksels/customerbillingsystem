#ifndef BILL_H
#define BILL_H

#include <QCoreApplication>

class Bill
{
private:
	int vadeMiktari;
	int odenenTutar;
	QString odemeTarihi;// gg/aa/yyyy
	int tc;
	int faturaNo;
public:
	Bill();
	void SetVadeMiktari(int vade);
	void SetOdenenTutar(int tutar);
	void SetOdemeTarihi(QString tarih);
	void SetTc(int tcno);
	void SetFaturaNo(int fNo);
	int GetVadeMiktari();
	int GetOdenenTutar();
	QString GetOdemeTarihi();
	int GetTc();
	int GetFaturaNo();
};

#endif // BILL_H
