#include "customer.h"

Customer::Customer()
{
	adi="";
	soyAdi="";
	adresi="";
	telNo="";
	tc=-1;
	toplamOdeme=0;
}
void Customer::SetAd(QString ad)
{
	adi=ad;
}
void Customer::SetAdres(QString adres)
{
	adresi=adres;
}
void Customer::SetSoyad(QString sad)
{
	soyAdi=sad;
}
void Customer::SetTelNo(QString tel)
{
	telNo=tel;
}
void Customer::SetMusteriTc(int tcno)
{
	tc=tcno;
}
void  Customer::SetToplamOdeme(int odeme)
{
	toplamOdeme+=odeme;
}

QString Customer::GetAd()
{
	return adi;
}
QString Customer::GetAdres()
{
	return adresi;
}
QString Customer::GetSoyad()
{
	return soyAdi;
}
QString Customer::GetTelNo()
{
	return telNo;
}
int Customer::GetMusteriTc()
{
	return tc;
}
int Customer::GetToplamOdeme()
{
	return toplamOdeme;
}
