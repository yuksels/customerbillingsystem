#include "bill.h"

Bill::Bill()
{
	vadeMiktari=-1;
	odenenTutar=-1;
	odemeTarihi="gg/aa/yyyy";
	tc=-1;
	faturaNo=-1;
}

void Bill::SetVadeMiktari(int vade)
{
	vadeMiktari=vade;
}
void Bill::SetOdenenTutar(int tutar)
{
	odenenTutar=tutar;
}
void Bill::SetOdemeTarihi(QString tarih)
{
	odemeTarihi=tarih;
}
void Bill::SetTc(int tcno)
{
	tc=tcno;
}
void Bill::SetFaturaNo(int fNo)
{
	faturaNo=fNo;
}
int Bill::GetVadeMiktari()
{
	return vadeMiktari;
}
int Bill::GetOdenenTutar()
{
	return odenenTutar;
}
QString Bill::GetOdemeTarihi()
{
	return odemeTarihi;
}
int Bill::GetTc()
{
	return tc;
}
int Bill::GetFaturaNo()
{
	return faturaNo;
}
