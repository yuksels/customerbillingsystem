#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <QCoreApplication>

class Customer
{
private:
	QString adi;
	QString soyAdi;
	QString adresi;
	QString telNo;
	int tc;
	int toplamOdeme;
public:
	Customer();
	void SetAd(QString ad);
	void SetSoyad(QString sad);
	void SetAdres(QString adres);
	void SetTelNo(QString tel);
	void SetMusteriTc(int tcno);
	void SetToplamOdeme(int odeme);
	int GetToplamOdeme();
	QString GetAd();
	QString GetSoyad();
	QString GetAdres();
	QString GetTelNo();
	int GetMusteriTc();
};

#endif // CUSTOMER_H
