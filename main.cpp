#include <QCoreApplication>
#include<bill.h>
#include<customer.h>
#include<iostream>
#include<QTextStream>
#include<QString>
#include<QDebug>

using namespace std;

//değişkenler
QList<Customer> ListMusteri;
QList<Bill> ListFatura;
int faturaNo=0;

// fonksiyonların ön bildirimleri

//menü
void Menu();

//müşteri işlemleri

// müşteri ekleme işlemleri
void MusteriEkle();
void EklemeM(Customer eklenecek);

//müşteri güncelleme işlemleri
void MusteriGuncelle();
void GuncellemeM(Customer guncellenecek,int eskiTc);

//müşteri silme işlemleri
void MusteriSil();
void SilM(Customer silinecek);

//müşteri listeleme işlemleri
void MusteriListelemeSecenekleri();
void MusteriListeleTc();
void MusteriListeleOdeme();
void MusteriListesiGoruntule();

//liste kontrolleri
bool MusteriListesiBosMu();
bool ListedeArananMusteriVarMi(Customer aracanakMusteri);
// bilgi girişi
Customer MusteriBilgileriGir(Customer musteri,bool yeniKayitMi);

// fatura ekleme işlemleri
void FaturaEkle();
void EklemeF(Bill eklenecek);

//fatura güncelleme işlemleri
void FaturaGuncelle();
void GuncellemeF(Bill guncellenecek,int eskiFatNo);

//fatura silme işlemleri
void FaturaSil();
void SilF(Bill silinecek);

//fatura listeleme işlemleri
void FaturaListelemeSecenekleri();
void FaturaListeleFatura();
void FaturaListeleOdeme();
void FaturaListesiGoruntule();

//liste kontrolleri
bool FaturaListesiBosMu();
bool ListedeArananFaturaVarMi(Bill faturaMusteri);

// bilgi girişi
Bill FaturaBilgileriGir(Bill fatura, bool yeniKayitMi);

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	cout<<"########################### Müşteri Faturalandırma Sistemi #############################"<<endl;
	Menu();
	//müşteri ekle
	//müşteri sil müşteriye ait faturaları da sil
	//müşteri güncelle müşteriye ait faturaları da güncelle
	//yeni fatura ekle
	//müşteri listele müşterkoduna göre,yapılan toplam ödeme miktarına göre (artan ve azalan)
	//fatura listele yıla göre,pahalıdan ucuza ucuzdan pahalıya
	return a.exec();
}

void Menu()
{
	int secim;
	cout<<" 0) Cikis \n 1) Musteri Ekle\n 2) Musteri Sil \n 3) Musteri Guncelle \n 4) Musteri Listele \n 5) Fatura Ekle \n 6) Fatura Guncelle(Tarihe gore) \n 7) Fatura Sil \n 8) Fatura Listele  \n\n";
	cin>>secim;

	switch (secim)
	{
	case 0:// çıkış
		exit(0);
		break;
	case 1:// MUSTERİ EKLE
		MusteriEkle();
		Menu();
		break;
	case 2:// MUSTERİ sil
		if(MusteriListesiBosMu()==true)
		{
			qDebug()<<"Silinecek Veri bulunmamaktadır!!";

		}
		else
		{
			MusteriSil();
		}
		Menu();
		break;
	case 3:// MUSTERİ guncelle

		if(MusteriListesiBosMu()==true)
		{
			qDebug()<<"Guncellenecek Veri bulunmamaktadır!!";

		}
		else
		{
			MusteriGuncelle();
		}
		Menu();
		break;
	case 4:// MUSTERİ listele
		if(MusteriListesiBosMu()==true)
		{
			qDebug()<<"Listelecenek Veri bulunmamaktadır!!";

		}
		else
		{
			MusteriListelemeSecenekleri();
		}
		Menu();

		break;
	case 5:// fatura EKLE
		FaturaEkle();
		break;
	case 6:// fatura sil

		if(FaturaListesiBosMu()==true)
		{
			qDebug()<<"Guncellenecek fatura bulunmamaktadır!!";

		}
		else
		{
			FaturaSil();
		}
		Menu();

		break;
	case 7:// fatura guncelle

		if(FaturaListesiBosMu()==true)
		{
			qDebug()<<"Guncellenecek fatura bulunmamaktadır!!";

		}
		else
		{
			FaturaGuncelle();
		}
		Menu();
		break;
	case 8:// fatura listele

		if(FaturaListesiBosMu()==true)
		{
			qDebug()<<"Listelecenek fatura bulunmamaktadır!!";

		}
		else
		{
			// LİSTELEME
			FaturaListelemeSecenekleri();
		}
		Menu();
		break;
	default:
		cout<<"Yanlis giris yaptiniz lutfen tekrar deneyiniz!!\a\a\a";
		Menu();
		break;
	}
}


/////////// müşteri işlemleri

void MusteriEkle()
{
	Customer yeniMusteri;
	qDebug()<<"Eklenecek ";
	yeniMusteri=MusteriBilgileriGir(yeniMusteri,false);

	if(ListedeArananMusteriVarMi(yeniMusteri)==true)
	{
		int secim;
		cout<<"Hata!!!\a\a";
		cout<<"Eklemek istediginiz musteri listede vardir\n\n 1) Tekrar ekleme\n 2) Bir ust menu\n 0) Cikis\n";

		while(true)
		{
			switch (secim)
			{
			case 1:
				MusteriEkle();
				break;
			case 2:
				Menu();
				break;
			case 0:
				exit(0);
				break;
			default:
				cout<<"Yanlis giris yaptiniz lutfen tekrar deneyiniz!!\a\a\a";
				break;
			}
		}
	}
	else
	{
		EklemeM(MusteriBilgileriGir(yeniMusteri,true));
	}

}
void EklemeM(Customer eklenecek)
{
	ListMusteri.append(eklenecek);
	qDebug()<<eklenecek.GetMusteriTc()<<"\n"<<eklenecek.GetAd()<<" "<<eklenecek.GetSoyad()<<"\n"<<eklenecek.GetAdres()<<"\n kaydı sisteme eklenmiştir.";
}
void GuncellemeM(Customer guncellenecek, int eskiTc)
{

	for(int i=0; i<ListMusteri.count(); i++)
	{
		if(ListMusteri[i].GetMusteriTc()==guncellenecek.GetMusteriTc())
		{
			ListMusteri.removeAt(i);
			ListMusteri.append(guncellenecek);
			qDebug()<<ListMusteri[i].GetMusteriTc()<<"\n"<<ListMusteri[i].GetAd()<<" "<<ListMusteri[i].GetSoyad()<<"\n"<<ListMusteri[i].GetAdres()<<"\n kayıt bilgileri.\n"<<endl;
			qDebug()<<guncellenecek.GetMusteriTc()<<"\n"<<guncellenecek.GetAd()<<" "<<guncellenecek.GetSoyad()<<"\n"<<guncellenecek.GetAdres()<<"\n olarak guncellendi"<<endl;
			break;
		}
	}
	for(int i=0;i<ListFatura.count();i++)
	{
		if(ListMusteri[i].GetMusteriTc()==eskiTc)
		{
			ListFatura[i].SetTc(guncellenecek.GetMusteriTc());
		}
	}


	//Customer *adresi=&guncellenecek;

	/*
	foreach (Customer listedekiMusteri, ListMusteri)
	{
		if(listedekiMusteri.GetMusteriTc()==guncellenecek.GetMusteriTc())
		{
			ListMusteri.removeOne(&listedekiMusteri);
			ListMusteri.append(guncellenecek);
			qDebug()<<listedekiMusteri.GetMusteriTc()<<"\n"<<listedekiMusteri.GetAd()<<" "<<listedekiMusteri.GetSoyad()<<"\n"<<listedekiMusteri.GetAdres()<<"\n kayıt bilgileri.\n"<<endl;
			qDebug()<<guncellenecek.GetMusteriTc()<<"\n"<<guncellenecek.GetAd()<<" "<<guncellenecek.GetSoyad()<<"\n"<<guncellenecek.GetAdres()<<"\n olarak guncellendi"<<endl;
			break;
		}

	}*/
}
void MusteriGuncelle()
{
	Customer guncellecekMusteri;


	cout<<"Guncellenecek ";
	guncellecekMusteri=MusteriBilgileriGir(guncellecekMusteri,false);
	if(ListedeArananMusteriVarMi(guncellecekMusteri)==false)
	{
		int secim;
		cout<<"Hata!!!\a\a";
		cout<<"Guncellemek istediginiz musteri listede yoktur\n\n 1) Tekrar guncelleme\n 2) Bir ust menu\n 0) Cikis\n";

		while(true)
		{
			switch (secim)
			{
			case 1:
				MusteriGuncelle();
				break;
			case 2:
				Menu();
				break;
			case 0:
				exit(0);
				break;
			default:
				cout<<"Yanlis giris yaptiniz lutfen tekrar deneyiniz!!\a\a\a";
				break;
			}
		}
	}
	else
	{
		GuncellemeM(MusteriBilgileriGir(guncellecekMusteri,true),guncellecekMusteri.GetMusteriTc());
	}
}
bool ListedeArananMusteriVarMi(Customer aracanakMusteri)
{
	for(int i=0; i<ListMusteri.count(); i++)
	{
		if(ListMusteri[i].GetMusteriTc()==aracanakMusteri.GetMusteriTc())
		{
			return true;
		}
	}
	/*foreach (Customer listedekiMusteri, ListMusteri)
	{
		if(listedekiMusteri.GetMusteriTc()==aracanakMusteri.GetMusteriTc())
		{
			return true;
		}
	}*/
	return false;
}
Customer MusteriBilgileriGir(Customer musteri,bool yeniKayitMi)
{
	QString adi;
	QString soyAdi;
	QString adresi;
	QString telNo;
	int tc;


	cout<<" Musteri Tc sini tekrar giriniz: ";
	cin>>tc;
	musteri.SetMusteriTc(tc);
	if(yeniKayitMi==true)
	{
		/*
	cout<<" Musteri adini giriniz: ";
	adi=abc.readLine();
	musteri.SetAd(adi);

	cout<<" Musteri soyadini giriniz: ";
	cin>>soyAdi;
	musteri.SetSoyad(soyAdi);

	cout<<" Musteri telefon numarasini giriniz: "<<endl;
	cin>>telNo;
	musteri.SetTelNo(telNo);

	cout<<" Musteri adresini giriniz: ";
	cin>>adresi;
	musteri.SetAdres(adresi);

	*/
	}
	return musteri;
}
void MusteriSil()
{
	Customer silinecekMusteri;

	silinecekMusteri=MusteriBilgileriGir(silinecekMusteri,false);
	cout<<"Silinecek ";

	if(ListedeArananMusteriVarMi(silinecekMusteri)==true)
	{
		int secim;
		cout<<"Hata!!!\a\a";
		cout<<"Silmek istediginiz musteri listede yoktur\n\n 1) Tekrar silme\n 2) Bir ust menu\n 0) Cikis\n";

		while(true)
		{
			switch (secim)
			{
			case 1:
				MusteriSil();
				break;
			case 2:
				Menu();
				break;
			case 0:
				exit(0);
				break;
			default:
				cout<<"Yanlis giris yaptiniz lutfen tekrar deneyiniz!!\a\a\a";
				break;
			}
		}
	}
	else
	{
		SilM(silinecekMusteri);
	}
}
void SilM(Customer silinecek)
{
	for(int i=0; i<ListMusteri.count(); i++)
	{
		if(ListMusteri[i].GetMusteriTc()==silinecek.GetMusteriTc())
		{
			ListMusteri.removeAt(i);
			break;
		}
	}
	for(int i=0;i<ListFatura.count();i++) // silinen müşteri(lerin) fatura(larının)sının da silinmesi
	{
		if(ListFatura[i].GetTc()==silinecek.GetMusteriTc())
		{
			ListFatura.removeAt(i);
		}
	}
}
void  MusteriListeleTc()
{
	// bubble sıralama

	Customer temp;
	for(int i=1;i<ListMusteri.count();i++)
	{
		for(int j=0;j<ListMusteri.count()-i;j++)
		{
			if(ListMusteri[j].GetMusteriTc()>ListMusteri[j+1].GetMusteriTc())
			{
				temp=ListMusteri[j];
				ListMusteri[j]=ListMusteri[j+1];
				ListMusteri[j+1]=temp;
			}
		}
	}
	qDebug()<<" Tc ye gore";
	MusteriListesiGoruntule();
}
void  MusteriListeleOdeme()
{
	Customer temp;
	for(int i=0;i<ListMusteri.count();i++)
	{
		for(int j=0;j<ListMusteri.count()-i;j++)
		{
			if(ListMusteri[j].GetToplamOdeme()>ListMusteri[j+1].GetToplamOdeme())
			{
				temp=ListMusteri[j];
				ListMusteri[j]=ListMusteri[j+1];
				ListMusteri[j+1]=temp;
			}
		}
	}
	qDebug()<<" Toplam Odemeye Gore";
	MusteriListesiGoruntule();
}
void MusteriListesiGoruntule()
{
	for(int i=ListMusteri.count()-1; i>=0; i--)
	{
		qDebug()<<" "<<ListMusteri[i].GetMusteriTc()<<"\n "<<ListMusteri[i].GetAd()<<" "<<ListMusteri[i].GetSoyad();
		qDebug()<<" "<<ListMusteri[i].GetTelNo()<<"\n "<<ListMusteri[i].GetAdres()<<"\nToplam Odeme===>>>\t"<<ListMusteri[i].GetToplamOdeme();
	}
}
void MusteriListelemeSecenekleri()
{	int secim;
	cout<<" 1) Tc ye gore listeleme\n 2) Odeme miktarina gore listeleme \n 3) Bir ust menu\n 0) Cikis\n";
	cin>>secim;
	switch (secim)
	{
	case 1:
		MusteriListeleTc();
		break;
	case 2:
		MusteriListeleOdeme();
		break;
	case 3:
		Menu();
		break;
	case 0:
		exit(0);
		break;
	default:
		cout<<"Yanlis giris yaptiniz lutfen tekrar deneyiniz!!\a\a\a";
		break;
	}
}
bool MusteriListesiBosMu()
{
	return ListMusteri.isEmpty();
}

////////// fatura işlemleri
void FaturaEkle()
{	Customer yeniMusteri;
	Bill yeniFatura;
	int tc;
	qDebug()<<" Fatura olusturmak istediginiz kisinin Tc nosunu giriniz\n";
	cin>>tc;
	yeniMusteri.SetMusteriTc(tc);

	if(ListedeArananMusteriVarMi(yeniMusteri))
	{
		EklemeF(FaturaBilgileriGir(yeniFatura,true));
		Menu();
	}
	else
	{
		qDebug()<<" Girdiginiz Tc nolu kisi sistemde kayitli degildir \n\n";
		Menu();
	}
}
Bill FaturaBilgileriGir(Bill fatura, bool yeniKayitMi)
{
	int fatNo;
	int tc;
	int vade;
	int miktar;
	QString tarih;

	cout<<" Fatura no giriniz giriniz: ";
	cin>>fatNo;
	fatura.SetFaturaNo(fatNo);
	if(yeniKayitMi==true)
	{
		cout<<" Musteri Tc sini giriniz: ";
		cin>>tc;
		fatura.SetTc(tc);
		/*	cout<<" Odeme tarihi giriniz(gg/aa/YYYY): ";
	cin>>tarih;
	fatura.SetOdemeTarihi(tarih);
*/
		cout<<" Odenen tutari giriniz: "<<endl;
		cin>>miktar;
		fatura.SetOdenenTutar(miktar);

		cout<<" Vade miktarini giriniz: ";
		cin>>vade;

		fatura.SetVadeMiktari(vade);
		fatura.SetFaturaNo(faturaNo++);
	}
	return fatura;
}
void EklemeF(Bill eklenecek)
{
	ListFatura.append(eklenecek);
}
bool ListedeArananFaturaVarMi(Bill faturaMusteri)
{
	for(int i=0;i<ListFatura.count();i++)
	{
		if(ListFatura[i].GetFaturaNo()==faturaMusteri.GetFaturaNo())
		{
			return true;
		}
	}
	return false;
}
void GuncellemeF(Bill guncellenecek, int eskiFatNo)
{
	for(int i=0; i<ListFatura.count(); i++)
	{
		if(ListFatura[i].GetFaturaNo()==eskiFatNo)
		{
			qDebug()<<ListFatura[i].GetFaturaNo()<<"\n tc:"<<ListFatura[i].GetTc()<<" "<<ListFatura[i].GetOdenenTutar()<<"\n"<<ListFatura[i].GetVadeMiktari()<<"\n "<<ListFatura[i].GetOdemeTarihi()<<"\n kayıt bilgileri.\n"<<endl;


			ListFatura.removeAt(i);
			ListFatura.append(guncellenecek);
			qDebug()<<guncellenecek.GetFaturaNo()<<"\n tc:"<<guncellenecek.GetTc()<<" "<<guncellenecek.GetOdenenTutar()<<"\n"<<guncellenecek.GetVadeMiktari()<<"\n "<<guncellenecek.GetOdemeTarihi()<<"\n olarak guncellendi\n"<<endl;

			for(int j=0 ;j<ListMusteri.count();j++) //tc bazlı  toplam tutar güncelleme
			{
				if(ListMusteri[j].GetMusteriTc()==guncellenecek.GetTc())
				{
					ListMusteri[j].SetToplamOdeme(ListMusteri[j].GetToplamOdeme()-ListFatura[i].GetOdenenTutar()+guncellenecek.GetOdenenTutar());
					break;
				}
			}
			break;
		}
	}

}
void FaturaGuncelle()
{
	Bill guncellecekFatura;


	cout<<"Guncellenecek ";
	guncellecekFatura=FaturaBilgileriGir(guncellecekFatura,false);
	if(ListedeArananFaturaVarMi(guncellecekFatura)==false)
	{
		int secim;
		cout<<"Hata!!!\a\a";
		cout<<"Guncellemek istediginiz musteri listede yoktur\n\n 1) Tekrar guncelleme\n 2) Bir ust menu\n 0) Cikis\n";

		while(true)
		{
			switch (secim)
			{
			case 1:
				MusteriGuncelle();
				break;
			case 2:
				Menu();
				break;
			case 0:
				exit(0);
				break;
			default:
				cout<<"Yanlis giris yaptiniz lutfen tekrar deneyiniz!!\a\a\a";
				break;
			}
		}
	}
	else
	{
		GuncellemeF(FaturaBilgileriGir(guncellecekFatura,true),guncellecekFatura.GetFaturaNo());
	}
}
void FaturaSil()
{
	Bill silinecekFatura;

	silinecekFatura=FaturaBilgileriGir(silinecekFatura,false);
	cout<<"Silinecek ";

	if(ListedeArananFaturaVarMi(silinecekFatura)==true)
	{
		int secim;
		cout<<"Hata!!!\a\a";
		cout<<"Silmek istediginiz fatura listede yoktur\n\n 1) Tekrar silme\n 2) Bir ust menu\n 0) Cikis\n";

		while(true)
		{
			switch (secim)
			{
			case 1:
				FaturaSil();
				break;
			case 2:
				Menu();
				break;
			case 0:
				exit(0);
				break;
			default:
				cout<<"Yanlis giris yaptiniz lutfen tekrar deneyiniz!!\a\a\a";
				break;
			}
		}
	}
	else
	{
		SilF(silinecekFatura);
	}
}
void SilF(Bill silinecek)
{
	for(int i=0; i<ListFatura.count(); i++)
	{
		if(ListFatura[i].GetTc()==silinecek.GetTc())
		{
			ListFatura.removeAt(i);
			break;
		}
	}
	for(int i=0;i<ListMusteri.count();i++) // silinen  fatura(larının)sının da  musteriden toplam ödemeden düşülmesi
	{
		if(ListMusteri[i].GetMusteriTc()==silinecek.GetTc())
		{
			ListMusteri[i].SetToplamOdeme( -(silinecek.GetOdenenTutar()));
		}
	}
}


void FaturaListelemeSecenekleri()
{
	int secim;
	cout<<" 1) Fatura No ya gore listeleme\n 2) Odeme miktarina gore listeleme \n 3) Bir ust menu\n 0) Cikis\n";
	cin>>secim;
	switch (secim)
	{
	case 1:
		FaturaListeleFatura();
		break;
	case 2:
		FaturaListeleOdeme();
		break;
	case 3:
		Menu();
		break;
	case 0:
		exit(0);
		break;
	default:
		cout<<"Yanlis giris yaptiniz lutfen tekrar deneyiniz!!\a\a\a";
		break;
	}
}
void FaturaListeleFatura()
{
	// bubble sıralama

	Bill temp;
	for(int i=1;i<ListFatura.count();i++)
	{
		for(int j=0;j<ListFatura.count()-i;j++)
		{
			if(ListFatura[j].GetFaturaNo()>ListFatura[j+1].GetFaturaNo())
			{
				temp=ListFatura[j];
				ListFatura[j]=ListFatura[j+1];
				ListFatura[j+1]=temp;
			}
		}
	}
	qDebug()<<" Fatura No ya gore";
	FaturaListesiGoruntule();
}
void FaturaListeleOdeme()
{
	// bubble sıralama

	Bill temp;
	for(int i=1;i<ListFatura.count();i++)
	{
		for(int j=0;j<ListFatura.count()-i;j++)
		{
			if(ListFatura[j].GetOdenenTutar()>ListFatura[j+1].GetOdenenTutar())
			{
				temp=ListFatura[j];
				ListFatura[j]=ListFatura[j+1];
				ListFatura[j+1]=temp;
			}
		}
	}
	qDebug()<<" Odeme miktarina gore";
	FaturaListesiGoruntule();
}
void FaturaListesiGoruntule()
{
	for(int i=ListFatura.count()-1; i>=0; i--)
	{
		qDebug()<<"  TC:"<<ListFatura[i].GetTc()<<"\n "<<" Fatura No: "<<ListFatura[i].GetFaturaNo()<<"\n Odeme Tarihi"<<ListFatura[i]. GetOdemeTarihi()<<" Tutar: "<<ListFatura[i].GetOdenenTutar()<<" Vade:"<<ListFatura[i].GetVadeMiktari();
	}
}
bool FaturaListesiBosMu()
{
	return ListFatura.isEmpty();
}
